.data
extern _func_addr: qword
.code
public _asm_jmp
_asm_jmp proc
	jmp [_func_addr];
_asm_jmp endp
end
