#include "persona.h"
#include "resource.h"
#include <Windows.h>
#include <string>
#include <bitset>
#include <dbghelp.h>
#pragma comment(lib, "dbghelp.lib")
using namespace std;

static float ieee754_to_dec(int num)
{
	float flt = 0;
	*((int*)&flt) = num;
	return flt;
}

static int dec_to_ieee754(float value)
{
	union
	{
		float input;
		int output;
	} data;
	data.input = value;
	bitset<sizeof(float)* CHAR_BIT> bits(data.output);
	string mystring = bits.to_string<char, char_traits<char>, allocator<char>>();
	bitset<32> set(mystring);
	return set.to_ulong();
}

static HMODULE GetCurrentModuleHandle()
{
	HMODULE hMod = NULL;
	GetModuleHandleExW(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
		reinterpret_cast<LPCWSTR>(&GetCurrentModuleHandle),
		&hMod);
	return hMod;
}

static bool SetResourceSectionWritable()
{
	uint CurrentResouceOffset = 0;
	uint CurrentResouceSize = 0;
	HMODULE handle = reinterpret_cast<HMODULE>((uint64)((uint64)GetCurrentModuleHandle >> 16) << 16);
	PIMAGE_NT_HEADERS NtHeader = ImageNtHeader(handle);
	WORD NumSections = NtHeader->FileHeader.NumberOfSections;
	PIMAGE_SECTION_HEADER Section = IMAGE_FIRST_SECTION(NtHeader);
	for (WORD i = 0; i < NumSections; i++)
	{
		if (memcmp(Section->Name, ".rsrc", 5) == 0)
		{
			CurrentResouceOffset = Section->VirtualAddress;
			if (Section->Misc.VirtualSize > Section->SizeOfRawData)
				CurrentResouceSize = Section->Misc.VirtualSize;
			else
				CurrentResouceSize = Section->SizeOfRawData;
			break;
		}
		Section++;
	}
	if (CurrentResouceOffset == 0 || CurrentResouceSize == 0)
		return false;
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, 0, GetCurrentProcessId());
	DWORD flRsrcNewProtect = PAGE_READWRITE;
	DWORD flRsrcOldProtect = 0;
	if (!VirtualProtectEx(hProcess, (LPVOID)((uint64)handle + CurrentResouceOffset), CurrentResouceSize, flRsrcNewProtect, &flRsrcOldProtect))
		return false;
	CloseHandle(hProcess);
	return true;
}

static void TranslateText(uint64 mHandle)
{
	if (!SetResourceSectionWritable())
		return;
	HMODULE handle = reinterpret_cast<HMODULE>((uint64)((uint64)GetCurrentModuleHandle >> 16) << 16);
	HRSRC file_resource[10] = {};
	bool zero = false;

	for (int i = 0; i < 10; i++)
	{
		file_resource[i] = FindResource(handle, MAKEINTRESOURCE(101 + i), L"BINARY_DATA");
		if (file_resource[i] == 0)
			return;
	}

	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, 0, GetCurrentProcessId());
	DWORD flSdataNewProtect = PAGE_READWRITE;
	DWORD flSdataOldProtect = 0;
	DWORD flSharedNewProtect = PAGE_EXECUTE_READWRITE;
	DWORD flSharedOldProtect = 0;
	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSdataOffset), SectionSdataSize, flSdataNewProtect, &flSdataOldProtect);
	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSharedOffset), SectionSharedSize, flSharedNewProtect, &flSharedOldProtect);

	*(uint64*)(mHandle + SectionSharedOffset + 0x3E364C) = 0x7E6F6644;	//Daytime
	HGLOBAL sd_addr = LoadResource(handle, file_resource[0]);
	HGLOBAL xpd_addr = LoadResource(handle, file_resource[5]);

	if (TranslateVersion == 1)
	{
		HGLOBAL sd_h_offset = LoadResource(handle, file_resource[1]);
		HGLOBAL sd_h_text = LoadResource(handle, file_resource[2]);
		HGLOBAL xpd_h_offset = LoadResource(handle, file_resource[6]);
		HGLOBAL xpd_h_text = LoadResource(handle, file_resource[7]);

		for (int i = 0; i <= 921; i++)
		{
			if (i <= 855)
				*(uint64*)(mHandle + SectionXpdataOffset + *(uint*)((uint64)sd_addr + (i * 4))) =
					(uint64)sd_h_text + *(uint*)((uint64)sd_h_offset + (i * 4));
			else if (i >= 856 && i <= 911)
				*(uint64*)(mHandle + SectionSdataOffset + *(uint*)((uint64)sd_addr + (i * 4))) =
					(uint64)sd_h_text + *(uint*)((uint64)sd_h_offset + (i * 4));
			else if (i >= 912)
			{
				zero = false;
				for (int j = 0; j < 30; j++)
				{
					if (*(ubyte*)(mHandle + SectionSdataOffset + *(uint*)((uint64)sd_addr + (i * 4)) + j) == (ubyte)0 && zero)
						break;
					if (*(ubyte*)((uint64)sd_h_text + *(uint*)((uint64)sd_h_offset + (i * 4)) + j) == (ubyte)0)
						zero = true;
					if (zero)
						*(ubyte*)(mHandle + SectionSdataOffset + *(uint*)((uint64)sd_addr + (i * 4)) + j) = (ubyte)0;
					else
						*(ubyte*)(mHandle + SectionSdataOffset + *(uint*)((uint64)sd_addr + (i * 4)) + j) =
							*(ubyte*)((uint64)sd_h_text + *(uint*)((uint64)sd_h_offset + (i * 4)) + j);
				}
			}
		}

		for (int i = 0; i <= 553; i++)
		{
			if (i <= 506)
			{
				zero = false;
				for (int j = 0; j < 63; j++)
				{
					if (*(ubyte*)(mHandle + SectionXpdataOffset + *(uint*)((uint64)xpd_addr + (i * 4)) + j) == (ubyte)0 && zero)
						break;
					if (*(ubyte*)((uint64)xpd_h_text + *(uint*)((uint64)xpd_h_offset + (i * 4)) + j) == (ubyte)0)
						zero = true;
					if (zero)
						*(ubyte*)(mHandle + SectionXpdataOffset + *(uint*)((uint64)xpd_addr + (i * 4)) + j) = (ubyte)0;
					else
						*(ubyte*)(mHandle + SectionXpdataOffset + *(uint*)((uint64)xpd_addr + (i * 4)) + j) =
							*(ubyte*)((uint64)xpd_h_text + *(uint*)((uint64)xpd_h_offset + (i * 4)) + j);
				}
			}
			else if (i >= 507 && i <= 544)
				*(uint64*)(mHandle + SectionXpdataOffset + *(uint*)((uint64)xpd_addr + (i * 4))) =
					(uint64)xpd_h_text + *(uint*)((uint64)xpd_h_offset + (i * 4));
			else if (i >= 545 && i <= 552)
				*(uint64*)(mHandle + SectionSdataOffset + *(uint*)((uint64)xpd_addr + (i * 4))) =
					(uint64)xpd_h_text + *(uint*)((uint64)xpd_h_offset + (i * 4));
			else if (i == 553)
				for (int j = 0; j < 191; j++)
					*(ubyte*)(mHandle + SectionXpdataOffset + *(uint*)((uint64)xpd_addr + (i * 4)) + j) =
						*(ubyte*)((uint64)xpd_h_text + *(uint*)((uint64)xpd_h_offset + (i * 4)) + j);
		}
	}
	else if (TranslateVersion == 2)
	{
		HGLOBAL sd_p_offset = LoadResource(handle, file_resource[3]);
		HGLOBAL sd_p_text = LoadResource(handle, file_resource[4]);
		HGLOBAL xpd_p_offset = LoadResource(handle, file_resource[8]);
		HGLOBAL xpd_p_text = LoadResource(handle, file_resource[9]);

		for (int i = 0; i <= 921; i++)
		{
			if (i <= 855)
				*(uint64*)(mHandle + SectionXpdataOffset + *(uint*)((uint64)sd_addr + (i * 4))) =
				(uint64)sd_p_text + *(uint*)((uint64)sd_p_offset + (i * 4));
			else if (i >= 856 && i <= 911)
				*(uint64*)(mHandle + SectionSdataOffset + *(uint*)((uint64)sd_addr + (i * 4))) =
				(uint64)sd_p_text + *(uint*)((uint64)sd_p_offset + (i * 4));
			else if (i >= 912)
			{
				zero = false;
				for (int j = 0; j < 30; j++)
				{
					if (*(ubyte*)(mHandle + SectionSdataOffset + *(uint*)((uint64)sd_addr + (i * 4)) + j) == (ubyte)0 && zero)
						break;
					if (*(ubyte*)((uint64)sd_p_text + *(uint*)((uint64)sd_p_offset + (i * 4)) + j) == (ubyte)0)
						zero = true;
					if (zero)
						*(ubyte*)(mHandle + SectionSdataOffset + *(uint*)((uint64)sd_addr + (i * 4)) + j) = (ubyte)0;
					else
						*(ubyte*)(mHandle + SectionSdataOffset + *(uint*)((uint64)sd_addr + (i * 4)) + j) =
						*(ubyte*)((uint64)sd_p_text + *(uint*)((uint64)sd_p_offset + (i * 4)) + j);
				}
			}
		}

		for (int i = 0; i <= 553; i++)
		{
			if (i <= 506)
			{
				zero = false;
				for (int j = 0; j < 63; j++)
				{
					if (*(ubyte*)(mHandle + SectionXpdataOffset + *(uint*)((uint64)xpd_addr + (i * 4)) + j) == (ubyte)0 && zero)
						break;
					if (*(ubyte*)((uint64)xpd_p_text + *(uint*)((uint64)xpd_p_offset + (i * 4)) + j) == (ubyte)0)
						zero = true;
					if (zero)
						*(ubyte*)(mHandle + SectionXpdataOffset + *(uint*)((uint64)xpd_addr + (i * 4)) + j) = (ubyte)0;
					else
						*(ubyte*)(mHandle + SectionXpdataOffset + *(uint*)((uint64)xpd_addr + (i * 4)) + j) =
						*(ubyte*)((uint64)xpd_p_text + *(uint*)((uint64)xpd_p_offset + (i * 4)) + j);
				}
			}
			else if (i >= 507 && i <= 544)
				*(uint64*)(mHandle + SectionXpdataOffset + *(uint*)((uint64)xpd_addr + (i * 4))) =
				(uint64)xpd_p_text + *(uint*)((uint64)xpd_p_offset + (i * 4));
			else if (i >= 545 && i <= 552)
				*(uint64*)(mHandle + SectionSdataOffset + *(uint*)((uint64)xpd_addr + (i * 4))) =
				(uint64)xpd_p_text + *(uint*)((uint64)xpd_p_offset + (i * 4));
			else if (i == 553)
				for (int j = 0; j < 191; j++)
					*(ubyte*)(mHandle + SectionXpdataOffset + *(uint*)((uint64)xpd_addr + (i * 4)) + j) =
					*(ubyte*)((uint64)xpd_p_text + *(uint*)((uint64)xpd_p_offset + (i * 4)) + j);
		}
	}

	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSdataOffset), SectionSdataSize, flSdataOldProtect, &flSdataNewProtect);
	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSharedOffset), SectionSharedSize, flSharedOldProtect, &flSharedNewProtect);
	CloseHandle(hProcess);
	return;
}

static void LockKeyboardLayout(uint64 mHandle)
{
	uint LockData[5] = { 0x909015EB, 0x190419B8, 0xB70F4404, 0x90E0EBC0, 0x90909090 };
	uint LockOffset[5] = { 0x4A0241, 0x4A0258, 0x4A025C, 0x4A0260, 0x4A0264 };
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, 0, GetCurrentProcessId());
	DWORD flSharedNewProtect = PAGE_EXECUTE_READWRITE;
	DWORD flSharedOldProtect = 0;
	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSharedOffset), SectionSharedSize, flSharedNewProtect, &flSharedOldProtect);
	for (int i = 0; i < 5; i++)
		*(uint*)(mHandle + SectionSharedOffset + LockOffset[i]) = LockData[i];
	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSharedOffset), SectionSharedSize, flSharedOldProtect, &flSharedNewProtect);
	CloseHandle(hProcess);
	return;
}

static void FixButtonLabel(uint64 mHandle)
{
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, 0, GetCurrentProcessId());
	DWORD flSdataNewProtect = PAGE_READWRITE;
	DWORD flSdataOldProtect = 0;
	DWORD flSharedNewProtect = PAGE_EXECUTE_READWRITE;
	DWORD flSharedOldProtect = 0;
	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSdataOffset), SectionSdataSize, flSdataNewProtect, &flSdataOldProtect);
	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSharedOffset), SectionSharedSize, flSharedNewProtect, &flSharedOldProtect);

	for (int i = 0; i < 27; i++)
	{
		for (int j = 0; j < 3; j++)
			*(uint*)(mHandle + SectionSdataOffset + SectionSdataSize - 0x400 + (i * 12) + (j * 4)) = NewButtonLabel[i][j];
		for (int j = 0; j < 2; j++)
			*(uint*)(mHandle + SectionSharedOffset + NewButtonJmp[i][0] + (j * 4)) = NewButtonJmp[i][j + 1];
	}
	for (int i = 0; i < 68; i++)
		*(uint*)(mHandle + SectionSharedOffset + 0x8855ED + (i * 4)) = NewButtonCode[i];

	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSdataOffset), SectionSdataSize, flSdataOldProtect, &flSdataNewProtect);
	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSharedOffset), SectionSharedSize, flSharedOldProtect, &flSharedNewProtect);
	CloseHandle(hProcess);
	return;
}

static void FixSpritePosition(uint64 mHandle)
{
	uint SpriteDefaultPosition[3] = { 0x43080000, 0x43AE8000, 0x43968000 };
	uint SpriteOffset[3] = { 0x167796, 0x16796E, 0x1573B8 };
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, 0, GetCurrentProcessId());
	DWORD flSharedNewProtect = PAGE_EXECUTE_READWRITE;
	DWORD flSharedOldProtect = 0;
	DWORD flSdataNewProtect = PAGE_READWRITE;
	DWORD flSdataOldProtect = 0;
	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSharedOffset), SectionSharedSize, flSharedNewProtect, &flSharedOldProtect);
	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSdataOffset), SectionSdataSize, flSdataNewProtect, &flSdataOldProtect);
	
	for (int i = 0; i < 3; i++)
	{
		SpriteNewPosition[i] += static_cast<int>(ieee754_to_dec(SpriteDefaultPosition[i]));
		*(uint*)(mHandle + SectionSdataOffset + SectionSdataSize - 0x280 + (i * 4)) = dec_to_ieee754(static_cast<float>(SpriteNewPosition[i]));
		*(uint*)(mHandle + SectionSharedOffset + SpriteOffset[i]) =
			(SectionSdataOffset + SectionSdataSize - 0x280 + (i * 4)) - (SectionSharedOffset + SpriteOffset[i] + 4);
	}
	
	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSdataOffset), SectionSdataSize, flSdataOldProtect, &flSdataNewProtect);
	VirtualProtectEx(hProcess, (LPVOID)(mHandle + SectionSharedOffset), SectionSharedSize, flSharedOldProtect, &flSharedNewProtect);
	CloseHandle(hProcess);
	return;
}

void CheckGameVersion()
{
	uint64 mHandle = (uint64)(GetModuleHandleA(NULL));
	int VersionOffset = 0x1E0;
	bool isVerified = true;
	uint pData = 0;

	for (int i = 0; i < 30; i++)
	{
		pData = *(uint*)(mHandle + VersionOffset + (i * 4));
		if (memcmp(&pData, &GameVersionData[i], 4) != 0)
		{
			isVerified = false;
			break;
		}
	}

	if (isVerified)
	{
		LockKeyboardLayout(mHandle);
		FixSpritePosition(mHandle);
		FixButtonLabel(mHandle);
		TranslateText(mHandle);
	}
	else
		MessageBoxA(NULL, "�� ������� ���������� ������ ����!", "������", MB_OK);

	return;
}