// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.
#include <Windows.h>
#include "persona.h"
typedef unsigned _int64 uint64;

extern "C"
{
	uint64 _func_addr = 0;
	void _asm_jmp();
}

struct winmm_dll {
	HMODULE dll;
	FARPROC OrignalCloseDriver;
	FARPROC OrignalDefDriverProc;
	FARPROC OrignalDriverCallback;
	FARPROC OrignalDrvGetModuleHandle;
	FARPROC OrignalGetDriverModuleHandle;
	FARPROC OrignalOpenDriver;
	FARPROC OrignalPlaySound;
	FARPROC OrignalPlaySoundA;
	FARPROC OrignalPlaySoundW;
	FARPROC OrignalSendDriverMessage;
	FARPROC OrignalWOWAppExit;
	FARPROC OrignalauxGetDevCapsA;
	FARPROC OrignalauxGetDevCapsW;
	FARPROC OrignalauxGetNumDevs;
	FARPROC OrignalauxGetVolume;
	FARPROC OrignalauxOutMessage;
	FARPROC OrignalauxSetVolume;
	FARPROC OrignaljoyConfigChanged;
	FARPROC OrignaljoyGetDevCapsA;
	FARPROC OrignaljoyGetDevCapsW;
	FARPROC OrignaljoyGetNumDevs;
	FARPROC OrignaljoyGetPos;
	FARPROC OrignaljoyGetPosEx;
	FARPROC OrignaljoyGetThreshold;
	FARPROC OrignaljoyReleaseCapture;
	FARPROC OrignaljoySetCapture;
	FARPROC OrignaljoySetThreshold;
	FARPROC OrignalmciDriverNotify;
	FARPROC OrignalmciDriverYield;
	FARPROC OrignalmciExecute;
	FARPROC OrignalmciFreeCommandResource;
	FARPROC OrignalmciGetCreatorTask;
	FARPROC OrignalmciGetDeviceIDA;
	FARPROC OrignalmciGetDeviceIDFromElementIDA;
	FARPROC OrignalmciGetDeviceIDFromElementIDW;
	FARPROC OrignalmciGetDeviceIDW;
	FARPROC OrignalmciGetDriverData;
	FARPROC OrignalmciGetErrorStringA;
	FARPROC OrignalmciGetErrorStringW;
	FARPROC OrignalmciGetYieldProc;
	FARPROC OrignalmciLoadCommandResource;
	FARPROC OrignalmciSendCommandA;
	FARPROC OrignalmciSendCommandW;
	FARPROC OrignalmciSendStringA;
	FARPROC OrignalmciSendStringW;
	FARPROC OrignalmciSetDriverData;
	FARPROC OrignalmciSetYieldProc;
	FARPROC OrignalmidiConnect;
	FARPROC OrignalmidiDisconnect;
	FARPROC OrignalmidiInAddBuffer;
	FARPROC OrignalmidiInClose;
	FARPROC OrignalmidiInGetDevCapsA;
	FARPROC OrignalmidiInGetDevCapsW;
	FARPROC OrignalmidiInGetErrorTextA;
	FARPROC OrignalmidiInGetErrorTextW;
	FARPROC OrignalmidiInGetID;
	FARPROC OrignalmidiInGetNumDevs;
	FARPROC OrignalmidiInMessage;
	FARPROC OrignalmidiInOpen;
	FARPROC OrignalmidiInPrepareHeader;
	FARPROC OrignalmidiInReset;
	FARPROC OrignalmidiInStart;
	FARPROC OrignalmidiInStop;
	FARPROC OrignalmidiInUnprepareHeader;
	FARPROC OrignalmidiOutCacheDrumPatches;
	FARPROC OrignalmidiOutCachePatches;
	FARPROC OrignalmidiOutClose;
	FARPROC OrignalmidiOutGetDevCapsA;
	FARPROC OrignalmidiOutGetDevCapsW;
	FARPROC OrignalmidiOutGetErrorTextA;
	FARPROC OrignalmidiOutGetErrorTextW;
	FARPROC OrignalmidiOutGetID;
	FARPROC OrignalmidiOutGetNumDevs;
	FARPROC OrignalmidiOutGetVolume;
	FARPROC OrignalmidiOutLongMsg;
	FARPROC OrignalmidiOutMessage;
	FARPROC OrignalmidiOutOpen;
	FARPROC OrignalmidiOutPrepareHeader;
	FARPROC OrignalmidiOutReset;
	FARPROC OrignalmidiOutSetVolume;
	FARPROC OrignalmidiOutShortMsg;
	FARPROC OrignalmidiOutUnprepareHeader;
	FARPROC OrignalmidiStreamClose;
	FARPROC OrignalmidiStreamOpen;
	FARPROC OrignalmidiStreamOut;
	FARPROC OrignalmidiStreamPause;
	FARPROC OrignalmidiStreamPosition;
	FARPROC OrignalmidiStreamProperty;
	FARPROC OrignalmidiStreamRestart;
	FARPROC OrignalmidiStreamStop;
	FARPROC OrignalmixerClose;
	FARPROC OrignalmixerGetControlDetailsA;
	FARPROC OrignalmixerGetControlDetailsW;
	FARPROC OrignalmixerGetDevCapsA;
	FARPROC OrignalmixerGetDevCapsW;
	FARPROC OrignalmixerGetID;
	FARPROC OrignalmixerGetLineControlsA;
	FARPROC OrignalmixerGetLineControlsW;
	FARPROC OrignalmixerGetLineInfoA;
	FARPROC OrignalmixerGetLineInfoW;
	FARPROC OrignalmixerGetNumDevs;
	FARPROC OrignalmixerMessage;
	FARPROC OrignalmixerOpen;
	FARPROC OrignalmixerSetControlDetails;
	FARPROC OrignalmmDrvInstall;
	FARPROC OrignalmmGetCurrentTask;
	FARPROC OrignalmmTaskBlock;
	FARPROC OrignalmmTaskCreate;
	FARPROC OrignalmmTaskSignal;
	FARPROC OrignalmmTaskYield;
	FARPROC OrignalmmioAdvance;
	FARPROC OrignalmmioAscend;
	FARPROC OrignalmmioClose;
	FARPROC OrignalmmioCreateChunk;
	FARPROC OrignalmmioDescend;
	FARPROC OrignalmmioFlush;
	FARPROC OrignalmmioGetInfo;
	FARPROC OrignalmmioInstallIOProcA;
	FARPROC OrignalmmioInstallIOProcW;
	FARPROC OrignalmmioOpenA;
	FARPROC OrignalmmioOpenW;
	FARPROC OrignalmmioRead;
	FARPROC OrignalmmioRenameA;
	FARPROC OrignalmmioRenameW;
	FARPROC OrignalmmioSeek;
	FARPROC OrignalmmioSendMessage;
	FARPROC OrignalmmioSetBuffer;
	FARPROC OrignalmmioSetInfo;
	FARPROC OrignalmmioStringToFOURCCA;
	FARPROC OrignalmmioStringToFOURCCW;
	FARPROC OrignalmmioWrite;
	FARPROC OrignalmmsystemGetVersion;
	FARPROC OrignalsndPlaySoundA;
	FARPROC OrignalsndPlaySoundW;
	FARPROC OrignaltimeBeginPeriod;
	FARPROC OrignaltimeEndPeriod;
	FARPROC OrignaltimeGetDevCaps;
	FARPROC OrignaltimeGetSystemTime;
	FARPROC OrignaltimeGetTime;
	FARPROC OrignaltimeKillEvent;
	FARPROC OrignaltimeSetEvent;
	FARPROC OrignalwaveInAddBuffer;
	FARPROC OrignalwaveInClose;
	FARPROC OrignalwaveInGetDevCapsA;
	FARPROC OrignalwaveInGetDevCapsW;
	FARPROC OrignalwaveInGetErrorTextA;
	FARPROC OrignalwaveInGetErrorTextW;
	FARPROC OrignalwaveInGetID;
	FARPROC OrignalwaveInGetNumDevs;
	FARPROC OrignalwaveInGetPosition;
	FARPROC OrignalwaveInMessage;
	FARPROC OrignalwaveInOpen;
	FARPROC OrignalwaveInPrepareHeader;
	FARPROC OrignalwaveInReset;
	FARPROC OrignalwaveInStart;
	FARPROC OrignalwaveInStop;
	FARPROC OrignalwaveInUnprepareHeader;
	FARPROC OrignalwaveOutBreakLoop;
	FARPROC OrignalwaveOutClose;
	FARPROC OrignalwaveOutGetDevCapsA;
	FARPROC OrignalwaveOutGetDevCapsW;
	FARPROC OrignalwaveOutGetErrorTextA;
	FARPROC OrignalwaveOutGetErrorTextW;
	FARPROC OrignalwaveOutGetID;
	FARPROC OrignalwaveOutGetNumDevs;
	FARPROC OrignalwaveOutGetPitch;
	FARPROC OrignalwaveOutGetPlaybackRate;
	FARPROC OrignalwaveOutGetPosition;
	FARPROC OrignalwaveOutGetVolume;
	FARPROC OrignalwaveOutMessage;
	FARPROC OrignalwaveOutOpen;
	FARPROC OrignalwaveOutPause;
	FARPROC OrignalwaveOutPrepareHeader;
	FARPROC OrignalwaveOutReset;
	FARPROC OrignalwaveOutRestart;
	FARPROC OrignalwaveOutSetPitch;
	FARPROC OrignalwaveOutSetPlaybackRate;
	FARPROC OrignalwaveOutSetVolume;
	FARPROC OrignalwaveOutUnprepareHeader;
	FARPROC OrignalwaveOutWrite;
} winmm;

_declspec() void FakeCloseDriver() { _func_addr = (uint64)winmm.OrignalCloseDriver; _asm_jmp(); }
_declspec() void FakeDefDriverProc() { _func_addr = (uint64)winmm.OrignalDefDriverProc; _asm_jmp(); }
_declspec() void FakeDriverCallback() { _func_addr = (uint64)winmm.OrignalDriverCallback; _asm_jmp(); }
_declspec() void FakeDrvGetModuleHandle() { _func_addr = (uint64)winmm.OrignalDrvGetModuleHandle; _asm_jmp(); }
_declspec() void FakeGetDriverModuleHandle() { _func_addr = (uint64)winmm.OrignalGetDriverModuleHandle; _asm_jmp(); }
_declspec() void FakeOpenDriver() { _func_addr = (uint64)winmm.OrignalOpenDriver; _asm_jmp(); }
_declspec() void FakePlaySound() { _func_addr = (uint64)winmm.OrignalPlaySound; _asm_jmp(); }
_declspec() void FakePlaySoundA() { _func_addr = (uint64)winmm.OrignalPlaySoundA; _asm_jmp(); }
_declspec() void FakePlaySoundW() { _func_addr = (uint64)winmm.OrignalPlaySoundW; _asm_jmp(); }
_declspec() void FakeSendDriverMessage() { _func_addr = (uint64)winmm.OrignalSendDriverMessage; _asm_jmp(); }
_declspec() void FakeWOWAppExit() { _func_addr = (uint64)winmm.OrignalWOWAppExit; _asm_jmp(); }
_declspec() void FakeauxGetDevCapsA() { _func_addr = (uint64)winmm.OrignalauxGetDevCapsA; _asm_jmp(); }
_declspec() void FakeauxGetDevCapsW() { _func_addr = (uint64)winmm.OrignalauxGetDevCapsW; _asm_jmp(); }
_declspec() void FakeauxGetNumDevs() { _func_addr = (uint64)winmm.OrignalauxGetNumDevs; _asm_jmp(); }
_declspec() void FakeauxGetVolume() { _func_addr = (uint64)winmm.OrignalauxGetVolume; _asm_jmp(); }
_declspec() void FakeauxOutMessage() { _func_addr = (uint64)winmm.OrignalauxOutMessage; _asm_jmp(); }
_declspec() void FakeauxSetVolume() { _func_addr = (uint64)winmm.OrignalauxSetVolume; _asm_jmp(); }
_declspec() void FakejoyConfigChanged() { _func_addr = (uint64)winmm.OrignaljoyConfigChanged; _asm_jmp(); }
_declspec() void FakejoyGetDevCapsA() { _func_addr = (uint64)winmm.OrignaljoyGetDevCapsA; _asm_jmp(); }
_declspec() void FakejoyGetDevCapsW() { _func_addr = (uint64)winmm.OrignaljoyGetDevCapsW; _asm_jmp(); }
_declspec() void FakejoyGetNumDevs() { _func_addr = (uint64)winmm.OrignaljoyGetNumDevs; _asm_jmp(); }
_declspec() void FakejoyGetPos() { _func_addr = (uint64)winmm.OrignaljoyGetPos; _asm_jmp(); }
_declspec() void FakejoyGetPosEx() { _func_addr = (uint64)winmm.OrignaljoyGetPosEx; _asm_jmp(); }
_declspec() void FakejoyGetThreshold() { _func_addr = (uint64)winmm.OrignaljoyGetThreshold; _asm_jmp(); }
_declspec() void FakejoyReleaseCapture() { _func_addr = (uint64)winmm.OrignaljoyReleaseCapture; _asm_jmp(); }
_declspec() void FakejoySetCapture() { _func_addr = (uint64)winmm.OrignaljoySetCapture; _asm_jmp(); }
_declspec() void FakejoySetThreshold() { _func_addr = (uint64)winmm.OrignaljoySetThreshold; _asm_jmp(); }
_declspec() void FakemciDriverNotify() { _func_addr = (uint64)winmm.OrignalmciDriverNotify; _asm_jmp(); }
_declspec() void FakemciDriverYield() { _func_addr = (uint64)winmm.OrignalmciDriverYield; _asm_jmp(); }
_declspec() void FakemciExecute() { _func_addr = (uint64)winmm.OrignalmciExecute; _asm_jmp(); }
_declspec() void FakemciFreeCommandResource() { _func_addr = (uint64)winmm.OrignalmciFreeCommandResource; _asm_jmp(); }
_declspec() void FakemciGetCreatorTask() { _func_addr = (uint64)winmm.OrignalmciGetCreatorTask; _asm_jmp(); }
_declspec() void FakemciGetDeviceIDA() { _func_addr = (uint64)winmm.OrignalmciGetDeviceIDA; _asm_jmp(); }
_declspec() void FakemciGetDeviceIDFromElementIDA() { _func_addr = (uint64)winmm.OrignalmciGetDeviceIDFromElementIDA; _asm_jmp(); }
_declspec() void FakemciGetDeviceIDFromElementIDW() { _func_addr = (uint64)winmm.OrignalmciGetDeviceIDFromElementIDW; _asm_jmp(); }
_declspec() void FakemciGetDeviceIDW() { _func_addr = (uint64)winmm.OrignalmciGetDeviceIDW; _asm_jmp(); }
_declspec() void FakemciGetDriverData() { _func_addr = (uint64)winmm.OrignalmciGetDriverData; _asm_jmp(); }
_declspec() void FakemciGetErrorStringA() { _func_addr = (uint64)winmm.OrignalmciGetErrorStringA; _asm_jmp(); }
_declspec() void FakemciGetErrorStringW() { _func_addr = (uint64)winmm.OrignalmciGetErrorStringW; _asm_jmp(); }
_declspec() void FakemciGetYieldProc() { _func_addr = (uint64)winmm.OrignalmciGetYieldProc; _asm_jmp(); }
_declspec() void FakemciLoadCommandResource() { _func_addr = (uint64)winmm.OrignalmciLoadCommandResource; _asm_jmp(); }
_declspec() void FakemciSendCommandA() { _func_addr = (uint64)winmm.OrignalmciSendCommandA; _asm_jmp(); }
_declspec() void FakemciSendCommandW() { _func_addr = (uint64)winmm.OrignalmciSendCommandW; _asm_jmp(); }
_declspec() void FakemciSendStringA() { _func_addr = (uint64)winmm.OrignalmciSendStringA; _asm_jmp(); }
_declspec() void FakemciSendStringW() { _func_addr = (uint64)winmm.OrignalmciSendStringW; _asm_jmp(); }
_declspec() void FakemciSetDriverData() { _func_addr = (uint64)winmm.OrignalmciSetDriverData; _asm_jmp(); }
_declspec() void FakemciSetYieldProc() { _func_addr = (uint64)winmm.OrignalmciSetYieldProc; _asm_jmp(); }
_declspec() void FakemidiConnect() { _func_addr = (uint64)winmm.OrignalmidiConnect; _asm_jmp(); }
_declspec() void FakemidiDisconnect() { _func_addr = (uint64)winmm.OrignalmidiDisconnect; _asm_jmp(); }
_declspec() void FakemidiInAddBuffer() { _func_addr = (uint64)winmm.OrignalmidiInAddBuffer; _asm_jmp(); }
_declspec() void FakemidiInClose() { _func_addr = (uint64)winmm.OrignalmidiInClose; _asm_jmp(); }
_declspec() void FakemidiInGetDevCapsA() { _func_addr = (uint64)winmm.OrignalmidiInGetDevCapsA; _asm_jmp(); }
_declspec() void FakemidiInGetDevCapsW() { _func_addr = (uint64)winmm.OrignalmidiInGetDevCapsW; _asm_jmp(); }
_declspec() void FakemidiInGetErrorTextA() { _func_addr = (uint64)winmm.OrignalmidiInGetErrorTextA; _asm_jmp(); }
_declspec() void FakemidiInGetErrorTextW() { _func_addr = (uint64)winmm.OrignalmidiInGetErrorTextW; _asm_jmp(); }
_declspec() void FakemidiInGetID() { _func_addr = (uint64)winmm.OrignalmidiInGetID; _asm_jmp(); }
_declspec() void FakemidiInGetNumDevs() { _func_addr = (uint64)winmm.OrignalmidiInGetNumDevs; _asm_jmp(); }
_declspec() void FakemidiInMessage() { _func_addr = (uint64)winmm.OrignalmidiInMessage; _asm_jmp(); }
_declspec() void FakemidiInOpen() { _func_addr = (uint64)winmm.OrignalmidiInOpen; _asm_jmp(); }
_declspec() void FakemidiInPrepareHeader() { _func_addr = (uint64)winmm.OrignalmidiInPrepareHeader; _asm_jmp(); }
_declspec() void FakemidiInReset() { _func_addr = (uint64)winmm.OrignalmidiInReset; _asm_jmp(); }
_declspec() void FakemidiInStart() { _func_addr = (uint64)winmm.OrignalmidiInStart; _asm_jmp(); }
_declspec() void FakemidiInStop() { _func_addr = (uint64)winmm.OrignalmidiInStop; _asm_jmp(); }
_declspec() void FakemidiInUnprepareHeader() { _func_addr = (uint64)winmm.OrignalmidiInUnprepareHeader; _asm_jmp(); }
_declspec() void FakemidiOutCacheDrumPatches() { _func_addr = (uint64)winmm.OrignalmidiOutCacheDrumPatches; _asm_jmp(); }
_declspec() void FakemidiOutCachePatches() { _func_addr = (uint64)winmm.OrignalmidiOutCachePatches; _asm_jmp(); }
_declspec() void FakemidiOutClose() { _func_addr = (uint64)winmm.OrignalmidiOutClose; _asm_jmp(); }
_declspec() void FakemidiOutGetDevCapsA() { _func_addr = (uint64)winmm.OrignalmidiOutGetDevCapsA; _asm_jmp(); }
_declspec() void FakemidiOutGetDevCapsW() { _func_addr = (uint64)winmm.OrignalmidiOutGetDevCapsW; _asm_jmp(); }
_declspec() void FakemidiOutGetErrorTextA() { _func_addr = (uint64)winmm.OrignalmidiOutGetErrorTextA; _asm_jmp(); }
_declspec() void FakemidiOutGetErrorTextW() { _func_addr = (uint64)winmm.OrignalmidiOutGetErrorTextW; _asm_jmp(); }
_declspec() void FakemidiOutGetID() { _func_addr = (uint64)winmm.OrignalmidiOutGetID; _asm_jmp(); }
_declspec() void FakemidiOutGetNumDevs() { _func_addr = (uint64)winmm.OrignalmidiOutGetNumDevs; _asm_jmp(); }
_declspec() void FakemidiOutGetVolume() { _func_addr = (uint64)winmm.OrignalmidiOutGetVolume; _asm_jmp(); }
_declspec() void FakemidiOutLongMsg() { _func_addr = (uint64)winmm.OrignalmidiOutLongMsg; _asm_jmp(); }
_declspec() void FakemidiOutMessage() { _func_addr = (uint64)winmm.OrignalmidiOutMessage; _asm_jmp(); }
_declspec() void FakemidiOutOpen() { _func_addr = (uint64)winmm.OrignalmidiOutOpen; _asm_jmp(); }
_declspec() void FakemidiOutPrepareHeader() { _func_addr = (uint64)winmm.OrignalmidiOutPrepareHeader; _asm_jmp(); }
_declspec() void FakemidiOutReset() { _func_addr = (uint64)winmm.OrignalmidiOutReset; _asm_jmp(); }
_declspec() void FakemidiOutSetVolume() { _func_addr = (uint64)winmm.OrignalmidiOutSetVolume; _asm_jmp(); }
_declspec() void FakemidiOutShortMsg() { _func_addr = (uint64)winmm.OrignalmidiOutShortMsg; _asm_jmp(); }
_declspec() void FakemidiOutUnprepareHeader() { _func_addr = (uint64)winmm.OrignalmidiOutUnprepareHeader; _asm_jmp(); }
_declspec() void FakemidiStreamClose() { _func_addr = (uint64)winmm.OrignalmidiStreamClose; _asm_jmp(); }
_declspec() void FakemidiStreamOpen() { _func_addr = (uint64)winmm.OrignalmidiStreamOpen; _asm_jmp(); }
_declspec() void FakemidiStreamOut() { _func_addr = (uint64)winmm.OrignalmidiStreamOut; _asm_jmp(); }
_declspec() void FakemidiStreamPause() { _func_addr = (uint64)winmm.OrignalmidiStreamPause; _asm_jmp(); }
_declspec() void FakemidiStreamPosition() { _func_addr = (uint64)winmm.OrignalmidiStreamPosition; _asm_jmp(); }
_declspec() void FakemidiStreamProperty() { _func_addr = (uint64)winmm.OrignalmidiStreamProperty; _asm_jmp(); }
_declspec() void FakemidiStreamRestart() { _func_addr = (uint64)winmm.OrignalmidiStreamRestart; _asm_jmp(); }
_declspec() void FakemidiStreamStop() { _func_addr = (uint64)winmm.OrignalmidiStreamStop; _asm_jmp(); }
_declspec() void FakemixerClose() { _func_addr = (uint64)winmm.OrignalmixerClose; _asm_jmp(); }
_declspec() void FakemixerGetControlDetailsA() { _func_addr = (uint64)winmm.OrignalmixerGetControlDetailsA; _asm_jmp(); }
_declspec() void FakemixerGetControlDetailsW() { _func_addr = (uint64)winmm.OrignalmixerGetControlDetailsW; _asm_jmp(); }
_declspec() void FakemixerGetDevCapsA() { _func_addr = (uint64)winmm.OrignalmixerGetDevCapsA; _asm_jmp(); }
_declspec() void FakemixerGetDevCapsW() { _func_addr = (uint64)winmm.OrignalmixerGetDevCapsW; _asm_jmp(); }
_declspec() void FakemixerGetID() { _func_addr = (uint64)winmm.OrignalmixerGetID; _asm_jmp(); }
_declspec() void FakemixerGetLineControlsA() { _func_addr = (uint64)winmm.OrignalmixerGetLineControlsA; _asm_jmp(); }
_declspec() void FakemixerGetLineControlsW() { _func_addr = (uint64)winmm.OrignalmixerGetLineControlsW; _asm_jmp(); }
_declspec() void FakemixerGetLineInfoA() { _func_addr = (uint64)winmm.OrignalmixerGetLineInfoA; _asm_jmp(); }
_declspec() void FakemixerGetLineInfoW() { _func_addr = (uint64)winmm.OrignalmixerGetLineInfoW; _asm_jmp(); }
_declspec() void FakemixerGetNumDevs() { _func_addr = (uint64)winmm.OrignalmixerGetNumDevs; _asm_jmp(); }
_declspec() void FakemixerMessage() { _func_addr = (uint64)winmm.OrignalmixerMessage; _asm_jmp(); }
_declspec() void FakemixerOpen() { _func_addr = (uint64)winmm.OrignalmixerOpen; _asm_jmp(); }
_declspec() void FakemixerSetControlDetails() { _func_addr = (uint64)winmm.OrignalmixerSetControlDetails; _asm_jmp(); }
_declspec() void FakemmDrvInstall() { _func_addr = (uint64)winmm.OrignalmmDrvInstall; _asm_jmp(); }
_declspec() void FakemmGetCurrentTask() { _func_addr = (uint64)winmm.OrignalmmGetCurrentTask; _asm_jmp(); }
_declspec() void FakemmTaskBlock() { _func_addr = (uint64)winmm.OrignalmmTaskBlock; _asm_jmp(); }
_declspec() void FakemmTaskCreate() { _func_addr = (uint64)winmm.OrignalmmTaskCreate; _asm_jmp(); }
_declspec() void FakemmTaskSignal() { _func_addr = (uint64)winmm.OrignalmmTaskSignal; _asm_jmp(); }
_declspec() void FakemmTaskYield() { _func_addr = (uint64)winmm.OrignalmmTaskYield; _asm_jmp(); }
_declspec() void FakemmioAdvance() { _func_addr = (uint64)winmm.OrignalmmioAdvance; _asm_jmp(); }
_declspec() void FakemmioAscend() { _func_addr = (uint64)winmm.OrignalmmioAscend; _asm_jmp(); }
_declspec() void FakemmioClose() { _func_addr = (uint64)winmm.OrignalmmioClose; _asm_jmp(); }
_declspec() void FakemmioCreateChunk() { _func_addr = (uint64)winmm.OrignalmmioCreateChunk; _asm_jmp(); }
_declspec() void FakemmioDescend() { _func_addr = (uint64)winmm.OrignalmmioDescend; _asm_jmp(); }
_declspec() void FakemmioFlush() { _func_addr = (uint64)winmm.OrignalmmioFlush; _asm_jmp(); }
_declspec() void FakemmioGetInfo() { _func_addr = (uint64)winmm.OrignalmmioGetInfo; _asm_jmp(); }
_declspec() void FakemmioInstallIOProcA() { _func_addr = (uint64)winmm.OrignalmmioInstallIOProcA; _asm_jmp(); }
_declspec() void FakemmioInstallIOProcW() { _func_addr = (uint64)winmm.OrignalmmioInstallIOProcW; _asm_jmp(); }
_declspec() void FakemmioOpenA() { _func_addr = (uint64)winmm.OrignalmmioOpenA; _asm_jmp(); }
_declspec() void FakemmioOpenW() { _func_addr = (uint64)winmm.OrignalmmioOpenW; _asm_jmp(); }
_declspec() void FakemmioRead() { _func_addr = (uint64)winmm.OrignalmmioRead; _asm_jmp(); }
_declspec() void FakemmioRenameA() { _func_addr = (uint64)winmm.OrignalmmioRenameA; _asm_jmp(); }
_declspec() void FakemmioRenameW() { _func_addr = (uint64)winmm.OrignalmmioRenameW; _asm_jmp(); }
_declspec() void FakemmioSeek() { _func_addr = (uint64)winmm.OrignalmmioSeek; _asm_jmp(); }
_declspec() void FakemmioSendMessage() { _func_addr = (uint64)winmm.OrignalmmioSendMessage; _asm_jmp(); }
_declspec() void FakemmioSetBuffer() { _func_addr = (uint64)winmm.OrignalmmioSetBuffer; _asm_jmp(); }
_declspec() void FakemmioSetInfo() { _func_addr = (uint64)winmm.OrignalmmioSetInfo; _asm_jmp(); }
_declspec() void FakemmioStringToFOURCCA() { _func_addr = (uint64)winmm.OrignalmmioStringToFOURCCA; _asm_jmp(); }
_declspec() void FakemmioStringToFOURCCW() { _func_addr = (uint64)winmm.OrignalmmioStringToFOURCCW; _asm_jmp(); }
_declspec() void FakemmioWrite() { _func_addr = (uint64)winmm.OrignalmmioWrite; _asm_jmp(); }
_declspec() void FakemmsystemGetVersion() { _func_addr = (uint64)winmm.OrignalmmsystemGetVersion; _asm_jmp(); }
_declspec() void FakesndPlaySoundA() { _func_addr = (uint64)winmm.OrignalsndPlaySoundA; _asm_jmp(); }
_declspec() void FakesndPlaySoundW() { _func_addr = (uint64)winmm.OrignalsndPlaySoundW; _asm_jmp(); }
_declspec() void FaketimeBeginPeriod() { _func_addr = (uint64)winmm.OrignaltimeBeginPeriod; _asm_jmp(); }
_declspec() void FaketimeEndPeriod() { _func_addr = (uint64)winmm.OrignaltimeEndPeriod; _asm_jmp(); }
_declspec() void FaketimeGetDevCaps() { _func_addr = (uint64)winmm.OrignaltimeGetDevCaps; _asm_jmp(); }
_declspec() void FaketimeGetSystemTime() { _func_addr = (uint64)winmm.OrignaltimeGetSystemTime; _asm_jmp(); }
_declspec() void FaketimeGetTime() { _func_addr = (uint64)winmm.OrignaltimeGetTime; _asm_jmp(); }
_declspec() void FaketimeKillEvent() { _func_addr = (uint64)winmm.OrignaltimeKillEvent; _asm_jmp(); }
_declspec() void FaketimeSetEvent() { _func_addr = (uint64)winmm.OrignaltimeSetEvent; _asm_jmp(); }
_declspec() void FakewaveInAddBuffer() { _func_addr = (uint64)winmm.OrignalwaveInAddBuffer; _asm_jmp(); }
_declspec() void FakewaveInClose() { _func_addr = (uint64)winmm.OrignalwaveInClose; _asm_jmp(); }
_declspec() void FakewaveInGetDevCapsA() { _func_addr = (uint64)winmm.OrignalwaveInGetDevCapsA; _asm_jmp(); }
_declspec() void FakewaveInGetDevCapsW() { _func_addr = (uint64)winmm.OrignalwaveInGetDevCapsW; _asm_jmp(); }
_declspec() void FakewaveInGetErrorTextA() { _func_addr = (uint64)winmm.OrignalwaveInGetErrorTextA; _asm_jmp(); }
_declspec() void FakewaveInGetErrorTextW() { _func_addr = (uint64)winmm.OrignalwaveInGetErrorTextW; _asm_jmp(); }
_declspec() void FakewaveInGetID() { _func_addr = (uint64)winmm.OrignalwaveInGetID; _asm_jmp(); }
_declspec() void FakewaveInGetNumDevs() { _func_addr = (uint64)winmm.OrignalwaveInGetNumDevs; _asm_jmp(); }
_declspec() void FakewaveInGetPosition() { _func_addr = (uint64)winmm.OrignalwaveInGetPosition; _asm_jmp(); }
_declspec() void FakewaveInMessage() { _func_addr = (uint64)winmm.OrignalwaveInMessage; _asm_jmp(); }
_declspec() void FakewaveInOpen() { _func_addr = (uint64)winmm.OrignalwaveInOpen; _asm_jmp(); }
_declspec() void FakewaveInPrepareHeader() { _func_addr = (uint64)winmm.OrignalwaveInPrepareHeader; _asm_jmp(); }
_declspec() void FakewaveInReset() { _func_addr = (uint64)winmm.OrignalwaveInReset; _asm_jmp(); }
_declspec() void FakewaveInStart() { _func_addr = (uint64)winmm.OrignalwaveInStart; _asm_jmp(); }
_declspec() void FakewaveInStop() { _func_addr = (uint64)winmm.OrignalwaveInStop; _asm_jmp(); }
_declspec() void FakewaveInUnprepareHeader() { _func_addr = (uint64)winmm.OrignalwaveInUnprepareHeader; _asm_jmp(); }
_declspec() void FakewaveOutBreakLoop() { _func_addr = (uint64)winmm.OrignalwaveOutBreakLoop; _asm_jmp(); }
_declspec() void FakewaveOutClose() { _func_addr = (uint64)winmm.OrignalwaveOutClose; _asm_jmp(); }
_declspec() void FakewaveOutGetDevCapsA() { _func_addr = (uint64)winmm.OrignalwaveOutGetDevCapsA; _asm_jmp(); }
_declspec() void FakewaveOutGetDevCapsW() { _func_addr = (uint64)winmm.OrignalwaveOutGetDevCapsW; _asm_jmp(); }
_declspec() void FakewaveOutGetErrorTextA() { _func_addr = (uint64)winmm.OrignalwaveOutGetErrorTextA; _asm_jmp(); }
_declspec() void FakewaveOutGetErrorTextW() { _func_addr = (uint64)winmm.OrignalwaveOutGetErrorTextW; _asm_jmp(); }
_declspec() void FakewaveOutGetID() { _func_addr = (uint64)winmm.OrignalwaveOutGetID; _asm_jmp(); }
_declspec() void FakewaveOutGetNumDevs() { _func_addr = (uint64)winmm.OrignalwaveOutGetNumDevs; _asm_jmp(); }
_declspec() void FakewaveOutGetPitch() { _func_addr = (uint64)winmm.OrignalwaveOutGetPitch; _asm_jmp(); }
_declspec() void FakewaveOutGetPlaybackRate() { _func_addr = (uint64)winmm.OrignalwaveOutGetPlaybackRate; _asm_jmp(); }
_declspec() void FakewaveOutGetPosition() { _func_addr = (uint64)winmm.OrignalwaveOutGetPosition; _asm_jmp(); }
_declspec() void FakewaveOutGetVolume() { _func_addr = (uint64)winmm.OrignalwaveOutGetVolume; _asm_jmp(); }
_declspec() void FakewaveOutMessage() { _func_addr = (uint64)winmm.OrignalwaveOutMessage; _asm_jmp(); }
_declspec() void FakewaveOutOpen() { _func_addr = (uint64)winmm.OrignalwaveOutOpen; _asm_jmp(); }
_declspec() void FakewaveOutPause() { _func_addr = (uint64)winmm.OrignalwaveOutPause; _asm_jmp(); }
_declspec() void FakewaveOutPrepareHeader() { _func_addr = (uint64)winmm.OrignalwaveOutPrepareHeader; _asm_jmp(); }
_declspec() void FakewaveOutReset() { _func_addr = (uint64)winmm.OrignalwaveOutReset; _asm_jmp(); }
_declspec() void FakewaveOutRestart() { _func_addr = (uint64)winmm.OrignalwaveOutRestart; _asm_jmp(); }
_declspec() void FakewaveOutSetPitch() { _func_addr = (uint64)winmm.OrignalwaveOutSetPitch; _asm_jmp(); }
_declspec() void FakewaveOutSetPlaybackRate() { _func_addr = (uint64)winmm.OrignalwaveOutSetPlaybackRate; _asm_jmp(); }
_declspec() void FakewaveOutSetVolume() { _func_addr = (uint64)winmm.OrignalwaveOutSetVolume; _asm_jmp(); }
_declspec() void FakewaveOutUnprepareHeader() { _func_addr = (uint64)winmm.OrignalwaveOutUnprepareHeader; _asm_jmp(); }
_declspec() void FakewaveOutWrite() { _func_addr = (uint64)winmm.OrignalwaveOutWrite; _asm_jmp(); }


BOOL APIENTRY DllMain( HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved )
{
	char path[MAX_PATH];
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		{
			CopyMemory(path + GetSystemDirectoryA(path, MAX_PATH - 11), "\\winmm.dll", 11);
			winmm.dll = LoadLibraryA(path);
			if (winmm.dll == false)
			{
				MessageBoxA(0, "Cannot load original winmm.dll library", "", MB_ICONERROR);
				ExitProcess(0);
			}
			winmm.OrignalCloseDriver = GetProcAddress(winmm.dll, "CloseDriver");
			winmm.OrignalDefDriverProc = GetProcAddress(winmm.dll, "DefDriverProc");
			winmm.OrignalDriverCallback = GetProcAddress(winmm.dll, "DriverCallback");
			winmm.OrignalDrvGetModuleHandle = GetProcAddress(winmm.dll, "DrvGetModuleHandle");
			winmm.OrignalGetDriverModuleHandle = GetProcAddress(winmm.dll, "GetDriverModuleHandle");
			winmm.OrignalOpenDriver = GetProcAddress(winmm.dll, "OpenDriver");
			winmm.OrignalPlaySound = GetProcAddress(winmm.dll, "PlaySound");
			winmm.OrignalPlaySoundA = GetProcAddress(winmm.dll, "PlaySoundA");
			winmm.OrignalPlaySoundW = GetProcAddress(winmm.dll, "PlaySoundW");
			winmm.OrignalSendDriverMessage = GetProcAddress(winmm.dll, "SendDriverMessage");
			winmm.OrignalWOWAppExit = GetProcAddress(winmm.dll, "WOWAppExit");
			winmm.OrignalauxGetDevCapsA = GetProcAddress(winmm.dll, "auxGetDevCapsA");
			winmm.OrignalauxGetDevCapsW = GetProcAddress(winmm.dll, "auxGetDevCapsW");
			winmm.OrignalauxGetNumDevs = GetProcAddress(winmm.dll, "auxGetNumDevs");
			winmm.OrignalauxGetVolume = GetProcAddress(winmm.dll, "auxGetVolume");
			winmm.OrignalauxOutMessage = GetProcAddress(winmm.dll, "auxOutMessage");
			winmm.OrignalauxSetVolume = GetProcAddress(winmm.dll, "auxSetVolume");
			winmm.OrignaljoyConfigChanged = GetProcAddress(winmm.dll, "joyConfigChanged");
			winmm.OrignaljoyGetDevCapsA = GetProcAddress(winmm.dll, "joyGetDevCapsA");
			winmm.OrignaljoyGetDevCapsW = GetProcAddress(winmm.dll, "joyGetDevCapsW");
			winmm.OrignaljoyGetNumDevs = GetProcAddress(winmm.dll, "joyGetNumDevs");
			winmm.OrignaljoyGetPos = GetProcAddress(winmm.dll, "joyGetPos");
			winmm.OrignaljoyGetPosEx = GetProcAddress(winmm.dll, "joyGetPosEx");
			winmm.OrignaljoyGetThreshold = GetProcAddress(winmm.dll, "joyGetThreshold");
			winmm.OrignaljoyReleaseCapture = GetProcAddress(winmm.dll, "joyReleaseCapture");
			winmm.OrignaljoySetCapture = GetProcAddress(winmm.dll, "joySetCapture");
			winmm.OrignaljoySetThreshold = GetProcAddress(winmm.dll, "joySetThreshold");
			winmm.OrignalmciDriverNotify = GetProcAddress(winmm.dll, "mciDriverNotify");
			winmm.OrignalmciDriverYield = GetProcAddress(winmm.dll, "mciDriverYield");
			winmm.OrignalmciExecute = GetProcAddress(winmm.dll, "mciExecute");
			winmm.OrignalmciFreeCommandResource = GetProcAddress(winmm.dll, "mciFreeCommandResource");
			winmm.OrignalmciGetCreatorTask = GetProcAddress(winmm.dll, "mciGetCreatorTask");
			winmm.OrignalmciGetDeviceIDA = GetProcAddress(winmm.dll, "mciGetDeviceIDA");
			winmm.OrignalmciGetDeviceIDFromElementIDA = GetProcAddress(winmm.dll, "mciGetDeviceIDFromElementIDA");
			winmm.OrignalmciGetDeviceIDFromElementIDW = GetProcAddress(winmm.dll, "mciGetDeviceIDFromElementIDW");
			winmm.OrignalmciGetDeviceIDW = GetProcAddress(winmm.dll, "mciGetDeviceIDW");
			winmm.OrignalmciGetDriverData = GetProcAddress(winmm.dll, "mciGetDriverData");
			winmm.OrignalmciGetErrorStringA = GetProcAddress(winmm.dll, "mciGetErrorStringA");
			winmm.OrignalmciGetErrorStringW = GetProcAddress(winmm.dll, "mciGetErrorStringW");
			winmm.OrignalmciGetYieldProc = GetProcAddress(winmm.dll, "mciGetYieldProc");
			winmm.OrignalmciLoadCommandResource = GetProcAddress(winmm.dll, "mciLoadCommandResource");
			winmm.OrignalmciSendCommandA = GetProcAddress(winmm.dll, "mciSendCommandA");
			winmm.OrignalmciSendCommandW = GetProcAddress(winmm.dll, "mciSendCommandW");
			winmm.OrignalmciSendStringA = GetProcAddress(winmm.dll, "mciSendStringA");
			winmm.OrignalmciSendStringW = GetProcAddress(winmm.dll, "mciSendStringW");
			winmm.OrignalmciSetDriverData = GetProcAddress(winmm.dll, "mciSetDriverData");
			winmm.OrignalmciSetYieldProc = GetProcAddress(winmm.dll, "mciSetYieldProc");
			winmm.OrignalmidiConnect = GetProcAddress(winmm.dll, "midiConnect");
			winmm.OrignalmidiDisconnect = GetProcAddress(winmm.dll, "midiDisconnect");
			winmm.OrignalmidiInAddBuffer = GetProcAddress(winmm.dll, "midiInAddBuffer");
			winmm.OrignalmidiInClose = GetProcAddress(winmm.dll, "midiInClose");
			winmm.OrignalmidiInGetDevCapsA = GetProcAddress(winmm.dll, "midiInGetDevCapsA");
			winmm.OrignalmidiInGetDevCapsW = GetProcAddress(winmm.dll, "midiInGetDevCapsW");
			winmm.OrignalmidiInGetErrorTextA = GetProcAddress(winmm.dll, "midiInGetErrorTextA");
			winmm.OrignalmidiInGetErrorTextW = GetProcAddress(winmm.dll, "midiInGetErrorTextW");
			winmm.OrignalmidiInGetID = GetProcAddress(winmm.dll, "midiInGetID");
			winmm.OrignalmidiInGetNumDevs = GetProcAddress(winmm.dll, "midiInGetNumDevs");
			winmm.OrignalmidiInMessage = GetProcAddress(winmm.dll, "midiInMessage");
			winmm.OrignalmidiInOpen = GetProcAddress(winmm.dll, "midiInOpen");
			winmm.OrignalmidiInPrepareHeader = GetProcAddress(winmm.dll, "midiInPrepareHeader");
			winmm.OrignalmidiInReset = GetProcAddress(winmm.dll, "midiInReset");
			winmm.OrignalmidiInStart = GetProcAddress(winmm.dll, "midiInStart");
			winmm.OrignalmidiInStop = GetProcAddress(winmm.dll, "midiInStop");
			winmm.OrignalmidiInUnprepareHeader = GetProcAddress(winmm.dll, "midiInUnprepareHeader");
			winmm.OrignalmidiOutCacheDrumPatches = GetProcAddress(winmm.dll, "midiOutCacheDrumPatches");
			winmm.OrignalmidiOutCachePatches = GetProcAddress(winmm.dll, "midiOutCachePatches");
			winmm.OrignalmidiOutClose = GetProcAddress(winmm.dll, "midiOutClose");
			winmm.OrignalmidiOutGetDevCapsA = GetProcAddress(winmm.dll, "midiOutGetDevCapsA");
			winmm.OrignalmidiOutGetDevCapsW = GetProcAddress(winmm.dll, "midiOutGetDevCapsW");
			winmm.OrignalmidiOutGetErrorTextA = GetProcAddress(winmm.dll, "midiOutGetErrorTextA");
			winmm.OrignalmidiOutGetErrorTextW = GetProcAddress(winmm.dll, "midiOutGetErrorTextW");
			winmm.OrignalmidiOutGetID = GetProcAddress(winmm.dll, "midiOutGetID");
			winmm.OrignalmidiOutGetNumDevs = GetProcAddress(winmm.dll, "midiOutGetNumDevs");
			winmm.OrignalmidiOutGetVolume = GetProcAddress(winmm.dll, "midiOutGetVolume");
			winmm.OrignalmidiOutLongMsg = GetProcAddress(winmm.dll, "midiOutLongMsg");
			winmm.OrignalmidiOutMessage = GetProcAddress(winmm.dll, "midiOutMessage");
			winmm.OrignalmidiOutOpen = GetProcAddress(winmm.dll, "midiOutOpen");
			winmm.OrignalmidiOutPrepareHeader = GetProcAddress(winmm.dll, "midiOutPrepareHeader");
			winmm.OrignalmidiOutReset = GetProcAddress(winmm.dll, "midiOutReset");
			winmm.OrignalmidiOutSetVolume = GetProcAddress(winmm.dll, "midiOutSetVolume");
			winmm.OrignalmidiOutShortMsg = GetProcAddress(winmm.dll, "midiOutShortMsg");
			winmm.OrignalmidiOutUnprepareHeader = GetProcAddress(winmm.dll, "midiOutUnprepareHeader");
			winmm.OrignalmidiStreamClose = GetProcAddress(winmm.dll, "midiStreamClose");
			winmm.OrignalmidiStreamOpen = GetProcAddress(winmm.dll, "midiStreamOpen");
			winmm.OrignalmidiStreamOut = GetProcAddress(winmm.dll, "midiStreamOut");
			winmm.OrignalmidiStreamPause = GetProcAddress(winmm.dll, "midiStreamPause");
			winmm.OrignalmidiStreamPosition = GetProcAddress(winmm.dll, "midiStreamPosition");
			winmm.OrignalmidiStreamProperty = GetProcAddress(winmm.dll, "midiStreamProperty");
			winmm.OrignalmidiStreamRestart = GetProcAddress(winmm.dll, "midiStreamRestart");
			winmm.OrignalmidiStreamStop = GetProcAddress(winmm.dll, "midiStreamStop");
			winmm.OrignalmixerClose = GetProcAddress(winmm.dll, "mixerClose");
			winmm.OrignalmixerGetControlDetailsA = GetProcAddress(winmm.dll, "mixerGetControlDetailsA");
			winmm.OrignalmixerGetControlDetailsW = GetProcAddress(winmm.dll, "mixerGetControlDetailsW");
			winmm.OrignalmixerGetDevCapsA = GetProcAddress(winmm.dll, "mixerGetDevCapsA");
			winmm.OrignalmixerGetDevCapsW = GetProcAddress(winmm.dll, "mixerGetDevCapsW");
			winmm.OrignalmixerGetID = GetProcAddress(winmm.dll, "mixerGetID");
			winmm.OrignalmixerGetLineControlsA = GetProcAddress(winmm.dll, "mixerGetLineControlsA");
			winmm.OrignalmixerGetLineControlsW = GetProcAddress(winmm.dll, "mixerGetLineControlsW");
			winmm.OrignalmixerGetLineInfoA = GetProcAddress(winmm.dll, "mixerGetLineInfoA");
			winmm.OrignalmixerGetLineInfoW = GetProcAddress(winmm.dll, "mixerGetLineInfoW");
			winmm.OrignalmixerGetNumDevs = GetProcAddress(winmm.dll, "mixerGetNumDevs");
			winmm.OrignalmixerMessage = GetProcAddress(winmm.dll, "mixerMessage");
			winmm.OrignalmixerOpen = GetProcAddress(winmm.dll, "mixerOpen");
			winmm.OrignalmixerSetControlDetails = GetProcAddress(winmm.dll, "mixerSetControlDetails");
			winmm.OrignalmmDrvInstall = GetProcAddress(winmm.dll, "mmDrvInstall");
			winmm.OrignalmmGetCurrentTask = GetProcAddress(winmm.dll, "mmGetCurrentTask");
			winmm.OrignalmmTaskBlock = GetProcAddress(winmm.dll, "mmTaskBlock");
			winmm.OrignalmmTaskCreate = GetProcAddress(winmm.dll, "mmTaskCreate");
			winmm.OrignalmmTaskSignal = GetProcAddress(winmm.dll, "mmTaskSignal");
			winmm.OrignalmmTaskYield = GetProcAddress(winmm.dll, "mmTaskYield");
			winmm.OrignalmmioAdvance = GetProcAddress(winmm.dll, "mmioAdvance");
			winmm.OrignalmmioAscend = GetProcAddress(winmm.dll, "mmioAscend");
			winmm.OrignalmmioClose = GetProcAddress(winmm.dll, "mmioClose");
			winmm.OrignalmmioCreateChunk = GetProcAddress(winmm.dll, "mmioCreateChunk");
			winmm.OrignalmmioDescend = GetProcAddress(winmm.dll, "mmioDescend");
			winmm.OrignalmmioFlush = GetProcAddress(winmm.dll, "mmioFlush");
			winmm.OrignalmmioGetInfo = GetProcAddress(winmm.dll, "mmioGetInfo");
			winmm.OrignalmmioInstallIOProcA = GetProcAddress(winmm.dll, "mmioInstallIOProcA");
			winmm.OrignalmmioInstallIOProcW = GetProcAddress(winmm.dll, "mmioInstallIOProcW");
			winmm.OrignalmmioOpenA = GetProcAddress(winmm.dll, "mmioOpenA");
			winmm.OrignalmmioOpenW = GetProcAddress(winmm.dll, "mmioOpenW");
			winmm.OrignalmmioRead = GetProcAddress(winmm.dll, "mmioRead");
			winmm.OrignalmmioRenameA = GetProcAddress(winmm.dll, "mmioRenameA");
			winmm.OrignalmmioRenameW = GetProcAddress(winmm.dll, "mmioRenameW");
			winmm.OrignalmmioSeek = GetProcAddress(winmm.dll, "mmioSeek");
			winmm.OrignalmmioSendMessage = GetProcAddress(winmm.dll, "mmioSendMessage");
			winmm.OrignalmmioSetBuffer = GetProcAddress(winmm.dll, "mmioSetBuffer");
			winmm.OrignalmmioSetInfo = GetProcAddress(winmm.dll, "mmioSetInfo");
			winmm.OrignalmmioStringToFOURCCA = GetProcAddress(winmm.dll, "mmioStringToFOURCCA");
			winmm.OrignalmmioStringToFOURCCW = GetProcAddress(winmm.dll, "mmioStringToFOURCCW");
			winmm.OrignalmmioWrite = GetProcAddress(winmm.dll, "mmioWrite");
			winmm.OrignalmmsystemGetVersion = GetProcAddress(winmm.dll, "mmsystemGetVersion");
			winmm.OrignalsndPlaySoundA = GetProcAddress(winmm.dll, "sndPlaySoundA");
			winmm.OrignalsndPlaySoundW = GetProcAddress(winmm.dll, "sndPlaySoundW");
			winmm.OrignaltimeBeginPeriod = GetProcAddress(winmm.dll, "timeBeginPeriod");
			winmm.OrignaltimeEndPeriod = GetProcAddress(winmm.dll, "timeEndPeriod");
			winmm.OrignaltimeGetDevCaps = GetProcAddress(winmm.dll, "timeGetDevCaps");
			winmm.OrignaltimeGetSystemTime = GetProcAddress(winmm.dll, "timeGetSystemTime");
			winmm.OrignaltimeGetTime = GetProcAddress(winmm.dll, "timeGetTime");
			winmm.OrignaltimeKillEvent = GetProcAddress(winmm.dll, "timeKillEvent");
			winmm.OrignaltimeSetEvent = GetProcAddress(winmm.dll, "timeSetEvent");
			winmm.OrignalwaveInAddBuffer = GetProcAddress(winmm.dll, "waveInAddBuffer");
			winmm.OrignalwaveInClose = GetProcAddress(winmm.dll, "waveInClose");
			winmm.OrignalwaveInGetDevCapsA = GetProcAddress(winmm.dll, "waveInGetDevCapsA");
			winmm.OrignalwaveInGetDevCapsW = GetProcAddress(winmm.dll, "waveInGetDevCapsW");
			winmm.OrignalwaveInGetErrorTextA = GetProcAddress(winmm.dll, "waveInGetErrorTextA");
			winmm.OrignalwaveInGetErrorTextW = GetProcAddress(winmm.dll, "waveInGetErrorTextW");
			winmm.OrignalwaveInGetID = GetProcAddress(winmm.dll, "waveInGetID");
			winmm.OrignalwaveInGetNumDevs = GetProcAddress(winmm.dll, "waveInGetNumDevs");
			winmm.OrignalwaveInGetPosition = GetProcAddress(winmm.dll, "waveInGetPosition");
			winmm.OrignalwaveInMessage = GetProcAddress(winmm.dll, "waveInMessage");
			winmm.OrignalwaveInOpen = GetProcAddress(winmm.dll, "waveInOpen");
			winmm.OrignalwaveInPrepareHeader = GetProcAddress(winmm.dll, "waveInPrepareHeader");
			winmm.OrignalwaveInReset = GetProcAddress(winmm.dll, "waveInReset");
			winmm.OrignalwaveInStart = GetProcAddress(winmm.dll, "waveInStart");
			winmm.OrignalwaveInStop = GetProcAddress(winmm.dll, "waveInStop");
			winmm.OrignalwaveInUnprepareHeader = GetProcAddress(winmm.dll, "waveInUnprepareHeader");
			winmm.OrignalwaveOutBreakLoop = GetProcAddress(winmm.dll, "waveOutBreakLoop");
			winmm.OrignalwaveOutClose = GetProcAddress(winmm.dll, "waveOutClose");
			winmm.OrignalwaveOutGetDevCapsA = GetProcAddress(winmm.dll, "waveOutGetDevCapsA");
			winmm.OrignalwaveOutGetDevCapsW = GetProcAddress(winmm.dll, "waveOutGetDevCapsW");
			winmm.OrignalwaveOutGetErrorTextA = GetProcAddress(winmm.dll, "waveOutGetErrorTextA");
			winmm.OrignalwaveOutGetErrorTextW = GetProcAddress(winmm.dll, "waveOutGetErrorTextW");
			winmm.OrignalwaveOutGetID = GetProcAddress(winmm.dll, "waveOutGetID");
			winmm.OrignalwaveOutGetNumDevs = GetProcAddress(winmm.dll, "waveOutGetNumDevs");
			winmm.OrignalwaveOutGetPitch = GetProcAddress(winmm.dll, "waveOutGetPitch");
			winmm.OrignalwaveOutGetPlaybackRate = GetProcAddress(winmm.dll, "waveOutGetPlaybackRate");
			winmm.OrignalwaveOutGetPosition = GetProcAddress(winmm.dll, "waveOutGetPosition");
			winmm.OrignalwaveOutGetVolume = GetProcAddress(winmm.dll, "waveOutGetVolume");
			winmm.OrignalwaveOutMessage = GetProcAddress(winmm.dll, "waveOutMessage");
			winmm.OrignalwaveOutOpen = GetProcAddress(winmm.dll, "waveOutOpen");
			winmm.OrignalwaveOutPause = GetProcAddress(winmm.dll, "waveOutPause");
			winmm.OrignalwaveOutPrepareHeader = GetProcAddress(winmm.dll, "waveOutPrepareHeader");
			winmm.OrignalwaveOutReset = GetProcAddress(winmm.dll, "waveOutReset");
			winmm.OrignalwaveOutRestart = GetProcAddress(winmm.dll, "waveOutRestart");
			winmm.OrignalwaveOutSetPitch = GetProcAddress(winmm.dll, "waveOutSetPitch");
			winmm.OrignalwaveOutSetPlaybackRate = GetProcAddress(winmm.dll, "waveOutSetPlaybackRate");
			winmm.OrignalwaveOutSetVolume = GetProcAddress(winmm.dll, "waveOutSetVolume");
			winmm.OrignalwaveOutUnprepareHeader = GetProcAddress(winmm.dll, "waveOutUnprepareHeader");
			winmm.OrignalwaveOutWrite = GetProcAddress(winmm.dll, "waveOutWrite");
			CheckGameVersion();
			break;
		}
		case DLL_PROCESS_DETACH:
		{
			FreeLibrary(winmm.dll);
		}
		break;
	}
	return TRUE;
}
